moviepy >= 1.0.2
PyYAML >= 3.10
google-api-python-client >= 1.3.1
Pillow == 9.5.0
numpy >= 1.8.2
unidecode >= 0.04.14
requests
google-auth
google-auth-oauthlib
