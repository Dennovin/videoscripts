jQuery.Color.fn.contrastColor = function () {
  var r = this._rgba[0],
    g = this._rgba[1],
    b = this._rgba[2];
  return (r * 299 + g * 587 + b * 144) / 1000 >= 131.5 ? "black" : "white";
};

var player;
var currentvideo = null,
  currentclip = {};
var storage = window.localStorage;
var videofiles = {};
var clips = [];
var zoomX, zoomY;

$(document).ready(function () {
  // Bind events
  $(".game-info").on("click", ".color", openColorSelector);
  $(".row").on("click", ".color-selection", selectColor);
  $(".files-loaded").on("click", ".row", selectVideoFile);
  $(".video-clips").on("click", ".selectable.row", selectClip);
  $(".video-clips").on("click", ".selectable.row .delete", deleteClip);
  $(".editbox.clip").on("change", "input, select", editClip);
  $(".boxes").on("click", ".title", toggleBox);
  $("body").click(removePopups);
  $("body").on("click", "#zoom-overlay.active", zoomClick);
  $("body").on("mousemove", "#zoom-overlay.active", zoomMove);
  $("body").on("mousedown", "#zoom-box.zoomed", zoomDrag);

  // Set up and resize player
  dummyPlayer = videojs("dummy-video-object");
  dummyPlayer.muted(true);

  player = videojs("video-object");
  resizePlayer();
  $(window).resize(resizePlayer);

  // Watch for files being dragged/dropped
  setupFileReader();

  // Initialize download links
  updateData();
});

function log(txt) {
  console.log(moment().format("HH:mm:ss.SSS") + " " + txt);
}

function toggleBox(e) {
  $(this).closest(".box").toggleClass("collapsed");
  e.preventDefault();
  e.stopPropagation();
}

function removePopups(e) {}

function formatTime(seconds) {
  var minutes = parseInt(seconds / 60);
  seconds = seconds % 60;

  var str = (minutes < 10 ? "0" : "") + minutes + ":";
  str += (seconds < 10 ? "0" : "") + parseInt(seconds) + ".";

  var cs = parseInt((seconds - parseInt(seconds)) * 100);
  str += (cs < 10 ? "0" : "") + cs;

  return str;
}

function getAbsoluteTime(time, videoIdx) {
  if (time === undefined) {
    time = player.currentTime();
  }

  if (videoIdx === undefined) {
    var selected = $(".files-loaded .selectable.selected");
    videoIdx = selected
      .closest(".files-loaded")
      .find(".selectable")
      .index(selected);
  }

  var activeSection = $(".files-loaded");
  activeSection.find(".selectable").each(function (i) {
    if (videoIdx <= i) {
      return false;
    }

    if (videofiles[$(this).attr("videourl")].duration !== undefined) {
      time += videofiles[$(this).attr("videourl")].duration;
    }
  });

  return time;
}

function getRelativeTime(time) {
  var videoIdx = 0;
  var section = $(".files-loaded");
  section.find(".selectable").each(function (i) {
    var duration = videofiles[$(this).attr("videourl")].duration;
    if (time < duration) {
      videoIdx = i;
      return false;
    }

    time -= duration;
  });

  return [videoIdx, time];
}

function parseTime(timestr) {
  return timestr
    .split(":")
    .reverse()
    .map(function (v, i) {
      return parseFloat(v) * Math.pow(60, i);
    })
    .reduce(function (p, c) {
      return p + c;
    });
}

function updateClipsLink() {
  data = "include:\n";
  for (var file of ["kq"]) {
    data += ` - ${file}.yaml\n`;
  }

  data += "\n";

  data += "cameras:\n";
  $(".files-loaded").each(function (i) {
    if ($(this).find(".selectable").length > 0) {
      data += "  - name: " + (i + 1) + "\n";
      data += "    files:\n";
      $(this)
        .find(".selectable")
        .each(function () {
          var videofile = videofiles[$(this).attr("videourl")];
          data += '      - "' + videofile.filename + '"\n';
        });

      data += "\n";
    }
  });

  data += "clips:\n";
  for (i in clips) {
    clip = clips[i];
    data += ` - { start: "${formatTime(clip.start)}", end: "${formatTime(
      clip.end
    )}", camera: "1"`;

    if (clip.tags) {
      data +=
        ", tags: [" + clip.tags.map((x) => '"' + x + '"').join(", ") + "]";
    }

    if (clip.zoom) {
      data +=
        ', pre_effects: [["crop", ' +
        clip.zoom.x1 +
        ", " +
        clip.zoom.y1 +
        ", " +
        clip.zoom.x2 +
        ", " +
        clip.zoom.y2 +
        '], ["resize", {"width": ' +
        player.videoWidth() +
        ', "height": ' +
        player.videoHeight() +
        "}]]";
    }

    if (clip.speed) {
      data += ', effects: [["speedx", ' + clip.speed + "]]";
    }

    data += " }\n";
  }

  var blob = new Blob([data], { type: "text/x-yaml" });
  $(".clips-link").attr("href", window.URL.createObjectURL(blob));
}

function getLocalStorageKey() {
  var filenames = [];
  for (i in videofiles) {
    filenames.push(videofiles[i].filename);
  }
  filenames.sort();

  return filenames.join(",");
}

function updateLocalStorage() {
  storageData = {
    clips: clips,
  };

  storage.setItem(getLocalStorageKey(), JSON.stringify(storageData));
}

function updateData() {
  updateClipsLink();
  updateLocalStorage();
}

function resizePlayer() {
  var newWidth, newHeight;

  newWidth = Math.min($(window).width() - 500, 1920);
  newHeight = Math.floor((newWidth * 9) / 16);

  $(".video-object")
    .attr("height", newHeight)
    .attr("width", newWidth)
    .css({ height: newHeight, width: newWidth });

  $(".boxes").css("left", newWidth + 20);
  $(".video-column").css("width", newWidth);
}

function updateCurrentClip() {
  if (currentclip.start) {
    $("#clip-start").val(formatTime(currentclip.start));
  } else {
    $("#clip-start").val("");
  }

  if (currentclip.end) {
    $("#clip-end").val(formatTime(currentclip.end));
  } else {
    $("#clip-end").val("");
  }

  if (currentclip.zoom) {
    $("#clip-zoom").val(
      formatLoc(currentclip.zoom.x1, currentclip.zoom.y1) +
        " - " +
        formatLoc(currentclip.zoom.x2, currentclip.zoom.y2)
    );
  } else {
    $("#clip-zoom").val("");
  }
}

function updateVideoFiles() {
  $(".files-loaded .inputs").not(".editbox .inputs").empty();

  var container = $(".files-loaded .inputs").not(".editbox .inputs");
  $.each(videofiles, function (i, videofile) {
    var row = $("<div/>")
      .addClass("row selectable")
      .attr("videourl", videofile.url);

    var textspan = $("<span/>").addClass("filename").text(videofile.filename);
    textspan.appendTo(row);

    row.appendTo(container);
  });

  if (currentvideo) {
    $(".files-loaded")
      .find(".row[videourl='" + currentvideo.url + "']")
      .addClass("selected");
  }
}

function updateClipList() {
  var container = $(".video-clips .inputs").not(".editbox .inputs");

  clips.sort(function (a, b) {
    return a.start - b.start;
  });
  $.each(clips, function (i, clip) {
    clip.idx = i;

    var row = container.find(".row[idx=" + clip.idx + "]");
    if (row.length == 0) {
      row = $("<div/>")
        .addClass("row selectable")
        .attr("idx", clip.idx)
        .appendTo(container);
    }

    var infocells = [
      $("<div/>").addClass("infocell").text(formatTime(clip.start)),
      $("<div/>").addClass("infocell").text(formatTime(clip.end)),
      $("<img/>").addClass("delete").attr("src", "delete.png"),
    ];

    row.empty().attr("start", clip.start).append(infocells);
  });

  var totalTime = clips
    .map((x) => x.end - x.start)
    .reduce(function (a, b) {
      return a + b;
    }, 0);
  $(".video-clips .title .info").html(formatTime(totalTime));
}

function forceUpdateClipList() {
  $(".editbox").detach().hide().appendTo("body");
  $(".video-clips .inputs").empty();
  updateClipList();
}

function openColorSelector(e) {
  e.stopPropagation();

  var $this = $(this);
  var $row = $this.closest(".row");
  var $selector = $(".color-selector").detach();
  if ($selector.length == 0) {
    $selector = $("<div/>").addClass("color-selector");

    for (i in colorList) {
      $("<div/>")
        .addClass("color-selection")
        .attr("background", colorList[i])
        .css("background", colorList[i])
        .appendTo($selector);
    }
  }

  $selector.css("top", $this.position().top + $this.height() + 8);
  $selector.find(".color-selection.selected").removeClass("selected");
  if ($this.val()) {
    $selector
      .find(".color-selection[background=" + $this.val() + "]")
      .addClass("selected");
  }

  $selector.appendTo($row);
}

function selectColor(e) {
  var $this = $(this);
  var $row = $this.closest(".row");
  var $selector = $this.closest(".color-selector");
  var $input = $row.find("input.color");

  e.stopPropagation();

  var color = $this.attr("background");
  $input.val(color).css({ background: color });
  $selector.remove();
}

function selectVideoFile() {
  var $this = $(this);
  var url = $this.attr("videourl");
  var videoIdx = $this.closest(".inputs").find(".selectable").index(this);
  var time = getAbsoluteTime(0, videoIdx);

  player.src(url);

  $(".files-loaded .inputs").find(".selectable").removeClass("selected");
  $this.addClass("selected");

  forceUpdateClipList();
}

function selectGoal() {
  var $this = $(this);

  var isSelected = $this.hasClass("selected");
  var editbox = $(".editbox.goal");
  var goal = goals[$this.attr("idx")];

  editbox.hide();
  $this.closest(".inputs").find(".selectable").removeClass("selected");

  if (!isSelected) {
    $this.addClass("selected");

    var offset = $this.offset();
    editbox.find("input[name=goal-time]").val(formatTime(goal.time));
    editbox.find("select[name=goal-team]").val(goal.team);
    editbox.detach().insertAfter($this).show();
  }
}

function deleteGoal(e) {
  var $this = $(this);
  var row = $this.closest(".row.goal");

  e.stopPropagation();

  goals.splice(row.attr("idx"), 1);
  updateData();
}

function deleteTimer(e) {
  var $this = $(this);
  var row = $this.closest(".row.timer");

  e.stopPropagation();

  timerEvents.splice(row.attr("idx"), 1);
  updateData();
}

function deleteClip(e) {
  var $this = $(this);
  var row = $this.closest(".row");

  e.stopPropagation();

  clips.splice(row.attr("idx"), 1);
  forceUpdateClipList();
  updateData();
}

function editGoal() {
  var selected = $(".timer-events .row.goal.selected");
  var goal = goals[selected.attr("idx")];

  $(".editbox.goal").find(".error").removeClass("error");

  try {
    goal.time = parseTime($(".editbox.goal input[name=goal-time]").val());
  } catch (err) {
    $(".editbox.goal input[name=goal-time]").addClass("error");
  }

  goal.team = $(".editbox.goal select[name=goal-team]").val();

  updateGameEvents();
  updateData();
}

function selectTimer() {
  var $this = $(this);

  var isSelected = $this.hasClass("selected");
  var editbox = $(".editbox.timer");
  var event = timerEvents[$this.attr("idx")];

  editbox.hide();
  $this.closest(".inputs").find(".selectable").removeClass("selected");

  if (!isSelected) {
    $this.addClass("selected");

    var offset = $this.offset();
    editbox.find("input[name=timer-time]").val(formatTime(event.time));
    editbox.find("input[name=timer-name]").val(event.timer);
    editbox.find("select[name=timer-event]").val(event.event);
    editbox.detach().insertAfter($this).show();
  }
}

function editTimer() {
  var selected = $(".timer-events .row.timer.selected");
  var event = timerEvents[selected.attr("idx")];

  $(".editbox.timer").find(".error").removeClass("error");

  try {
    event.time = parseTime($(".editbox.timer input[name=timer-time]").val());
  } catch (err) {
    $(".editbox.timer input[name=timer-time]").addClass("error");
  }

  event.timer = $(".editbox.timer input[name=timer-name]").val();
  event.event = $(".editbox.timer select[name=timer-event]").val();

  updateGameEvents();
  updateData();
}

function selectClip() {
  var $this = $(this);

  var isSelected = $this.hasClass("selected");
  var editbox = $(".editbox.clip");
  var clip = clips[$this.attr("idx")];

  editbox.hide();
  $this.closest(".inputs").find(".selectable").removeClass("selected");

  if (!isSelected) {
    $this.addClass("selected");

    var offset = $this.offset();
    editbox.find("input[name=clip-start-time]").val(formatTime(clip.start));
    editbox.find("input[name=clip-end-time]").val(formatTime(clip.end));
    editbox.detach().insertAfter($this).show();
  }
}

function editClip() {
  var selected = $(".video-clips .row.selected");
  var clip = clips[selected.attr("idx")];

  $(".editbox.clip").find(".error").removeClass("error");

  try {
    clip.start = parseTime(
      $(".editbox.clip input[name=clip-start-time]").val()
    );
  } catch (err) {
    $(".editbox.clip input[name=clip-start-time]").addClass("error");
  }

  try {
    clip.end = parseTime($(".editbox.clip input[name=clip-end-time]").val());
  } catch (err) {
    $(".editbox.clip input[name=clip-end-time]").addClass("error");
  }

  updateClipList();
  updateData();
}

function saveCurrentClip() {
  if (currentclip.start && currentclip.end) {
    currentclip.speed = $("#clip-speed").val();
    if ($("#clip-tags").val()) {
      currentclip.tags = $("#clip-tags")
        .val()
        .split(/\s*\,\s*/);
    }

    clips.push(currentclip);

    currentclip = {};
    $("#clip-tags").val("");
    $("#clip-speed").val("");
    $("#zoom-box").removeClass("zoomed");

    updateClipList();
    updateCurrentClip();
    updateData();
  }
}

function addTimerEvent(eventtime) {
  var timerNameIndex = 0;
  var timerEvent = "start";

  if (timerEvents.length > 0) {
    var lastEvent = timerEvents[timerEvents.length - 1];
    timerNameIndex = timerNames.indexOf(lastEvent.timer);

    if (timerNameIndex < timerNames.length - 1 && lastEvent.event == "end") {
      timerNameIndex++;
    } else if (lastEvent.event == "start" || lastEvent.event == "unpause") {
      if (
        timerNameIndex < timerNames.length - 1 &&
        eventtime - lastEvent.time > currentLocation.timerLength + 60
      ) {
        timerNameIndex++;
      } else if (
        eventtime - lastEvent.time <
        currentLocation.timerLength - 60
      ) {
        timerEvent = "pause";
      } else {
        timerEvent = "end";
      }
    } else if (lastEvent.event == "pause") {
      timerEvent = "unpause";
    }
  }

  var event = {
    timer: timerNames[timerNameIndex],
    event: timerEvent,
    time: eventtime,
  };
  timerEvents.push(event);
  updateGameEvents();
  updateData();
}

function addGoal(eventtime) {
  for (i in goals) {
    var goal = goals[i];
    if (Math.abs(goal.time - eventtime) < 5) {
      goal.team = goal.team == "home" ? "away" : "home";
      updateGameEvents();
      updateData();

      return;
    }
  }

  var event = { team: "away", time: eventtime };
  goals.push(event);
  updateGameEvents();
  updateData();
}

function getVideoDurations() {
  $("body").addClass("loading");

  for (url in videofiles) {
    if (videofiles[url].duration === undefined) {
      dummyPlayer.off("loadedmetadata");
      $("#dummy-video-object").find("video").get(0).src = url;
      dummyPlayer.on("loadedmetadata", function () {
        videofiles[url].duration = dummyPlayer.duration();
        log(`getVideoDurations: ${url} = ${videofiles[url].duration}`);
        getVideoDurations();
      });

      return;
    }
  }

  $("body").removeClass("loading");
  if (!currentvideo) {
    $(".files-loaded .inputs .row").first().click();
  }

  log(`getVideoDurations done`);
}

function divDrop(e) {
  e.preventDefault();

  e.dropEffect = "link";

  var reader = new FileReader();
  $.each(e.originalEvent.dataTransfer.files, function (i, file) {
    var videourl = window.URL.createObjectURL(file);
    var videofile = { filename: file.name, url: videourl };

    videofiles[videourl] = videofile;
  });

  updateVideoFiles();

  if (getLocalStorageKey() in storage) {
    storageData = JSON.parse(storage.getItem(getLocalStorageKey()));

    clips = storageData.clips;

    forceUpdateClipList();
  }

  getVideoDurations();
}

function zoomClick(e) {
  e.stopPropagation();
  e.preventDefault();

  if ($("#zoom-box").hasClass("active")) {
    var $videoObj = $(".video-object-active"),
      $zoomBox = $("#zoom-box");
    var x1 = $zoomBox.offset().left - $videoObj.offset().left,
      y1 = $zoomBox.offset().top - $videoObj.offset().top;
    var x2 = x1 + $zoomBox.width(),
      y2 = y1 + $zoomBox.height();

    x1 = Math.floor(((x1 / $videoObj.width()) * player.videoWidth()) / 16) * 16;
    x2 = Math.floor(((x2 / $videoObj.width()) * player.videoWidth()) / 16) * 16;
    y1 = Math.floor(((y1 / $videoObj.height()) * player.videoHeight()) / 9) * 9;
    y2 = y1 + (x2 - x1) * (9 / 16);

    currentclip.zoom = { x1: x1, x2: x2, y1: y1, y2: y2 };
    updateCurrentClip();

    $("#zoom-overlay").removeClass("active");
    $("#zoom-box").addClass("zoomed");
  } else {
    zoomX = e.pageX;
    zoomY = e.pageY;

    $("#zoom-box").css({
      left: e.pageX,
      top: e.pageY,
      width: 0,
      height: 0,
    });
  }

  $("#zoom-box").toggleClass("active");
}

function zoomDrag(e) {
  e.preventDefault();
  e.stopPropagation();

  let startPos = { x: e.pageX, y: e.pageY },
    startZoom = { x: currentclip.zoom.x1, y: currentclip.zoom.y1 },
    zoomSize = {
      x: currentclip.zoom.x2 - currentclip.zoom.x1,
      y: currentclip.zoom.y2 - currentclip.zoom.y1,
    },
    startCSS = $("#zoom-box").position(),
    $videoObj = $(".video-object-active");

  $("body").on("mousemove", "#zoom-box, .video-object", (e) => {
    let posDiff = { x: e.pageX - startPos.x, y: e.pageY - startPos.y };

    posDiff.x =
      Math.floor(((posDiff.x / $videoObj.width()) * player.videoWidth()) / 16) *
      16;
    posDiff.y =
      Math.floor(
        ((posDiff.y / $videoObj.height()) * player.videoHeight()) / 9
      ) * 9;

    currentclip.zoom.x1 = startZoom.x + posDiff.x;
    currentclip.zoom.y1 = startZoom.y + posDiff.y;
    currentclip.zoom.x2 = currentclip.zoom.x1 + zoomSize.x;
    currentclip.zoom.y2 = currentclip.zoom.y1 + zoomSize.y;
    updateCurrentClip();

    $("#zoom-box").css({
      left:
        startCSS.left +
        (posDiff.x * $videoObj.width()) / player.videoWidth() -
        2,
      top:
        startCSS.top +
        (posDiff.y * $videoObj.height()) / player.videoHeight() -
        2,
    });
  });

  $("body").one("mouseup", (e) => {
    e.preventDefault();
    e.stopPropagation();

    $("body").off("mousemove", "#zoom-box, .video-object");
  });
}

function zoomMove(e) {
  if ($("#zoom-box").hasClass("active")) {
    var $videoObj = $(".video-object-active");
    var vminX = $videoObj.offset().left,
      vminY = $videoObj.offset().top;
    var vmaxX = vminX + $videoObj.width(),
      vmaxY = vminY + $videoObj.height();
    var evtX = Math.min(vmaxX, Math.max(vminX, e.pageX));
    var evtY = Math.min(vmaxY, Math.max(vminY, e.pageY));
    var aspect = $videoObj.width() / $videoObj.height();
    var boxWidth = Math.abs(zoomX - evtX);
    var boxHeight = Math.abs(zoomY - evtY);

    if (aspect < boxWidth / boxHeight) {
      boxHeight = boxWidth / aspect;
    } else {
      boxWidth = boxHeight * aspect;
    }

    var boxLeft =
      evtX < zoomX
        ? Math.max(vminX, zoomX - boxWidth)
        : Math.min(vmaxX - boxWidth, zoomX);
    var boxTop =
      evtY < zoomY
        ? Math.max(vminY, zoomY - boxHeight)
        : Math.min(vmaxY - boxHeight, zoomY);

    $("#zoom-box").css({
      left: boxLeft - 2,
      top: boxTop - 2,
      width: boxWidth - 4,
      height: boxHeight - 4,
    });
  }
}

function zoomReset() {
  $("#zoom-box").removeClass("zoomed");
  currentclip.zoom = null;
}

function toggleZoom(e) {
  e.stopPropagation();
  e.preventDefault();

  if (
    $("#zoom-box").hasClass("active") ||
    $("#zoom-box").hasClass("zoomed") ||
    $("#zoom-overlay").hasClass("active")
  ) {
    zoomReset();
    updateCurrentClip();
    $("#zoom-box, #zoom-overlay").removeClass("active");
    return;
  }

  $("#zoom-overlay").addClass("active");
}

function setTime(time) {
  player.currentTime(time);
}

function timeToMove(e) {
  if (e.ctrlKey) {
    return 30;
  }

  if (e.shiftKey) {
    return 1;
  }

  return 5;
}

let controls = [
  {
    key: 73,
    button: "clip-start",
    action: () => {
      // I
      currentclip.start = getAbsoluteTime();
      updateCurrentClip();
    },
  },

  {
    key: 79,
    button: "clip-end",
    action: () => {
      // O
      currentclip.end = getAbsoluteTime();
      updateCurrentClip();
    },
  },

  { key: 65, button: "save-clip", action: saveCurrentClip }, // S

  {
    key: 70,
    action: () => {
      // F
      if (player.isFullscreen()) {
        player.exitFullscreen();
      } else {
        player.requestFullscreen();
      }
    },
  },

  {
    key: 190,
    button: "faster",
    action: () => {
      // >
      player.playbackRate(player.playbackRate() * 1.5);
    },
  },

  {
    key: 188,
    button: "slower",
    action: () => {
      // <
      player.playbackRate(player.playbackRate() / 1.5);
    },
  },

  {
    key: 191,
    button: "reset-speed",
    action: () => {
      // /
      player.playbackRate(1.0);
    },
  },

  {
    key: 219,
    button: "move-back",
    action: () => {
      // [
      if (currentclip && currentclip.start) {
        currentclip.start -= 1;
        $("#clip-start").val(formatTime(currentclip.start));
      }
    },
  },

  {
    key: 221,
    button: "move-forward",
    action: () => {
      // ]
      if (currentclip && currentclip.start) {
        currentclip.start += 1;
        $("#clip-start").val(formatTime(currentclip.start));
      }
    },
  },

  {
    key: 32,
    button: "play-pause",
    action: () => {
      // space
      var paused = player.paused();
      paused ? player.play() : player.pause();
    },
  },

  {
    key: 39,
    button: "step-forward",
    action: (e) => {
      // right
      setTime(player.currentTime() + timeToMove(e));
    },
  },

  {
    key: 37,
    button: "step-back",
    action: (e) => {
      // left
      setTime(player.currentTime() - timeToMove(e));
    },
  },

  {
    key: 38,
    button: "prev-file",
    action: (e) => {
      // up
      e.stopPropagation();
      e.preventDefault();

      var selection = $(".files-loaded .inputs .selected");
      if (selection.length == 0) {
        return;
      }

      var rows = selection.closest(".inputs").find(".selectable");
      var idx = rows.index(selection);
      if (idx <= 0) {
        return;
      }

      selectVideoFile.apply(rows[idx - 1]);
    },
  },

  {
    key: 40,
    button: "next-file",
    action: (e) => {
      // down
      e.stopPropagation();
      e.preventDefault();

      var selection = $(".files-loaded .inputs .selected");
      if (selection.length == 0) {
        return;
      }

      var rows = selection.closest(".inputs").find(".selectable");
      var idx = rows.index(selection);
      if (idx < 0 || idx >= rows.length - 1) {
        return;
      }

      selectVideoFile.apply(rows[idx + 1]);
    },
  },
];

function clickButton(e) {
  for (let control of controls) {
    if ($(this).hasClass(control.button)) {
      control.action(e);
    }
  }
}

function readKey(e) {
  for (let control of controls) {
    if (e.which == control.key) {
      control.action(e);
    }
  }
}

function setupFileReader() {
  $(document)
    .bind("dragover", function (e) {
      e.preventDefault();
    })
    .bind("dragenter", function (e) {
      e.preventDefault();
    })
    .bind("drop", divDrop);

  $("input").keydown(function (e) {
    e.stopPropagation();
  });
  $("html").keydown(readKey);
  $(".buttons span").click(clickButton);
}
